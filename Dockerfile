# specify the node base image with your desired version node:<version>
FROM node:8.9.4

WORKDIR /opt/service

COPY app.js ./app.js
COPY assets ./assets
COPY css ./css
COPY img ./img
COPY js ./js

COPY game-config.html ./game-config.html
COPY game-dialog.html ./game-dialog.html
COPY game-sidebar.html ./game-sidebar.html
COPY stride.js ./stride.js

COPY app-descriptor.json ./app-descriptor.json

COPY package.json ./package.json
COPY package-lock.json ./package-lock.json

RUN npm install

# replace this with your application's default port
EXPOSE 8080

ENTRYPOINT ["npm", "start"]
