const _ = require('lodash');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const cors = require('cors');
const Document = require('adf-builder').Document;
const prettyjson = require('prettyjson');
const request = require('request');
require('dotenv').config();
const app = express();
app.use(bodyParser.json());
app.use(express.static('.'));

function prettify_json(data, options = {}) {
  return '{\n' + prettyjson.render(data, options) + '\n}';
}

const { CLIENT_ID, CLIENT_SECRET, NODE_ENV, PORT} = process.env;
const API_BASE_URL = 'https://api.atlassian.com';

function getAccessToken(callback) {
  const options = {
    uri: 'https://auth.atlassian.com/oauth/token',
    method: 'POST',
    json: {
      grant_type: "client_credentials",
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      "audience": "api.atlassian.com"
    }
  };
  request(options, function (err, response, body) {
    if (response.statusCode === 200 && body.access_token) {
        console.log(`GetAccessToken request: ${options}`);
      callback(null, body.access_token);
    } else {
      callback("could not generate access token: " + JSON.stringify(response));
    }
  });
}

function sendMessage(cloudId, conversationId, messageTxt, callback) {
  getAccessToken(function (err, accessToken) {
    if (err) {
      callback(err);
    } else {
      const uri = API_BASE_URL + '/site/' + cloudId + '/conversation/' + conversationId + '/message';
      const options = {
        uri: uri,
        method: 'POST',
        headers: {
          authorization: "Bearer " + accessToken,
          "cache-control": "no-cache"
        },
        json: {
          body: {
            version: 1,
            type: "doc",
            content: [
              {
                type: "paragraph",
                content: [
                  {
                    type: "text",
                    text: messageTxt
                  }
                ]
              }
            ]
          }
        }
      }

      request(options, function (err, response, body) {
          console.log(`SendMessage request: ${options}`);
        callback(err, body);
      });
    }
  });
}


app.get('/healthcheck', function (req, res) {
  res.sendStatus(200);
});

app.post('/installed',
  function (req, res) {
    console.log('app installed in a conversation');
    const cloudId = req.body.cloudId;
    const conversationId = req.body.resourceId;
    sendMessage(cloudId, conversationId, "Hi there! Thanks for adding me to this conversation. To see me in action, just mention me in a message", function (err, response) {
      if (err)
        console.log(err);
    });
    res.sendStatus(204);
  }
);

/**
 * Simple library that wraps the Stride REST API
 */
const stride = require('./stride').factory({
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  env: NODE_ENV
});

app.post('/bot-mention',
  function (req, res) {
    console.log('bot mention');
    let data = req.body;
    let message = data.message;

    console.log(message.text.split(' '));
    sendReply(req.body, "Hey, what's up? (Sorry, that's all I can do)", function (err, response) {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        res.sendStatus(204);
      }
    });
  }
);

/*
 * This adds a glance to the sidebar. When the user clicks on it, Stride opens the module whose key is specified in "target".
 *
 * When a user first opens a Stride conversation where the app is installed,
 * the Stride app makes a REST call to the queryURL to get the initial value for the glance.
 * You can then update the glance for a conversation at any time by making a REST call to Stride.
 * Stride will then make sure glances are updated for all connected Stride users.
 */

app.get('/feels/glance',
  // cross domain request
  cors(),
  stride.validateJWT,
  (req, res) => {
    res.send(
      JSON.stringify({
        "label": {
          "value": "Atlassian Feels"
        }
      }));
  }
);

/**
* When a user clicks on the glance, Stride opens an iframe in the sidebar, and loads a page from your app,
* from the URL specified in the app descriptor
**/

app.get('/feels/sidebar',
  stride.validateJWT,
  (req, res) => {
    console.log('loading sidebar..');
    res.redirect("/feels-sidebar.html");
  }
);

async function getIssue(options) {
  console.log('Getting issue with: ' + prettify_json(options));
  let issues = await request(options, function (error, response, body) {
      if (error) throw new Error(error);
    });

  return issues;
}


function objectifyForm(formArray) {//serialize data function
  var returnArray = {};
  for (var i = 0; i < formArray.length; i++){
    returnArray[formArray[i]['name']] = formArray[i]['value'];
  }
  return returnArray;
}

const JIRA = {instance: 'https://extranet.atlassian.com/jira',
    //acalantog as admin
    auth: 'Basic ZWFjai1mZWVscy1ib3Q6RXk7RX1nYUJ6V3Q4Sj12O3NNNmJ2WVle',
    projectId: '10002',
    projectKey: 'DGR',
    issueTypeId: '10002',
    createIssue: '/rest/api/2/issue',
    search: '/rest/api/2/search',
    questJql: 'project = DGR AND resolution = Unresolved AND labels = quest order by lastViewed DESC',
    leaderBoardJql: '',

    feels: {
        instance: 'https://extranet.atlassian.com/jira',
        //feels-jira-bot
        auth: 'Basic ZWFjai1mZWVscy1ib3Q6RXk7RX1nYUJ6V3Q4Sj12O3NNNmJ2WVle',
        //FEELS
        projectId: '28841',
        projectKey: 'FEELS',
        //Feels Request
        issueTypeId: '82', //Story
        createIssue: '/rest/api/2/issue',
        charityName: 'summary',
        amount: 'customfield_10402',
        currency: 'customfield_10100',
        financeUrl: 'customfield_10003',
        foundationCountry: 'customfield_11203',
        office: 'customfield_11203',
        initiative: 'customfield_11301',
    }
};

let officeMap = new Map();
officeMap.set('34768', 'Amsterdam');
officeMap.set('34769', 'Austin');
officeMap.set('34774', 'Bengaluru');
officeMap.set('34770', 'Manila');
officeMap.set('34771', 'Mountain View');
officeMap.set('34772', 'San Francisco');
officeMap.set('34773', 'Sydney');
officeMap.set('34775', 'Remote');

let ratingMap = new Map();
ratingMap.set('1', '34776');
ratingMap.set('2', '34777');
ratingMap.set('3', '34778');
ratingMap.set('4', '34779');

app.post('/feels/submit',
  stride.validateJWT,
  (req, res) => {
    console.log('Received a call from the app frontend for donation ' + prettify_json(req.body));

    let data = objectifyForm(req.body);
    let reqBody = req.body;

    console.log(data);
    console.log(res.locals.context);

    const cloudId = res.locals.context.cloudId;
    const conversationId = res.locals.context.conversationId;
    const userId = res.locals.context.userId;

    let user;
    let ticketUrl;

    reqBody.cloudId = res.locals.context.cloudId
    reqBody.conversation = {};
    reqBody.conversation.id = res.locals.context.conversationId

    stride.replyWithText({reqBody, text: "Trying to submit.."})
            .then(() => res.sendStatus(200))
            .then(submitMood(data))
            .catch(err => console.error('  Something went wrong', prettify_json(err)));

    async function submitMood(data) {
      console.log('Inside submit async');

      let callback = async function() {
        success();
      }

      createJiraIssue(data, callback);
    }

    async function success() {
      console.log('successfulSubmitCard');
      //user = await stride.getUser({cloudId, userId});

      const doc = new Document();

      const card = doc.applicationCard('Success!')
        .link(ticketUrl)
        .description('Thanks for your participation and for being open! See you tomorrow!');
      card.detail()
        .title('Type')
        .text('Mood')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        });
      card.detail()
        .title('Office')
        .text(data.country);
        card.detail()
            .title('Rate')
            .text(data.rate);
        card.detail()
            .title('Comment')
            .text(data.comment);

      const document = doc.toJSON();

      console.log('Replying to Stride with card: ' + JSON.stringify(document));
      await stride.reply({reqBody, document});
    }

    async function createJiraIssue(data, callback) {
      user = await stride.getUser({cloudId, userId});
      console.log('User: '+prettify_json(user));


      let officeName = officeMap.get(data.office);
      let ratingId = ratingMap.get(data.rate);

      var options = { method: 'POST',
                      url: JIRA.feels.instance+JIRA.feels.createIssue,
                      headers:
                      { 'cache-control': 'no-cache',
                        'content-type': 'application/json',
                        authorization: JIRA.feels.auth },
                      body:
                          {
                              fields:
                                  {
                                      project: {id: JIRA.feels.projectId},
                                      issuetype: { id: JIRA.feels.issueTypeId },
                                      description: data.comment, //comment,
                                      summary: 'Feels',
                                      customfield_14784: {value: officeName, id: data.office},//Location - 14784
                                      customfield_32081: {value: data.rate, id: ratingId}//, //Rating - 32081
                                      //customfield_28585: user.emails[0].value //Reference Number - 28585
                                  }
                          },
                      json: true };
        console.log(`options: ${JSON.stringify(options)}`)

        ticketUrl = await request(options, function (error, response, body) {
          if (error) throw new Error(error);

          ticketUrl = JIRA.feels.instance+'/browse/'+body.key;
          console.log(ticketUrl);
          callback();

        });

    }
  }
);


/* All Quest related */

app.post('/ui/ping',
  stride.validateJWT,
  (req, res) => {
    console.log('Received a call from the app frontend ' + prettify_json(req.body));
    const cloudId = res.locals.context.cloudId;
    const conversationId = res.locals.context.conversationId;

    console.log('cloudId='+cloudId +'; conversationId='+conversationId);
    stride.sendTextMessage({cloudId, conversationId, text: "Pong"})
      .then(() => res.send(JSON.stringify({status: "Pong"})))
      .catch(() => res.send(JSON.stringify({status: "Failed"})))
  }
);

async function showGameCard({reqBody}) {
  console.log('loading game card..');

  //await stride.replyWithText({reqBody, text: "Sending a message with plenty of formatting..."});
  await addCard()

  async function addCard() {
    doc
    .paragraph()
    .text("And a card");
    const card = doc.applicationCard('With a title')
      .link('https://www.atlassian.com')
      .description('With some description, and a couple of attributes');
    card.detail()
      .title('Type')
      .text('Task')
      .icon({
        url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
        label: 'Task'
      });
    card.detail()
      .title('User')
      .text('Joe Blog')
      .icon({
        url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
        label: 'Task'
      });

    const document = doc.toJSON();

    await stride.reply({reqBody, document});
  }
}

/**
 * Don't worry about this for now.
 */
app.get('/descriptor', function (req, res) {
  fs.readFile('./app-descriptor.json', function (err, descriptorTemplate) {
    const template = _.template(descriptorTemplate);
    const descriptor = template({
      host: 'https://' + req.headers.host
    });
    res.set('Content-Type', 'application/json');
    res.send(descriptor);
  });
});

http.createServer(app).listen(PORT, function () {
  console.log(`App running on port ${PORT}`);
});
